# Generated by Django 3.0.5 on 2020-05-06 03:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('konsultasicabang', '0005_auto_20200505_0203'),
    ]

    operations = [
        migrations.CreateModel(
            name='Administrator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(default='', max_length=50)),
                ('password', models.CharField(default='', max_length=50)),
                ('nomor_identitas', models.CharField(default='', max_length=50)),
                ('nama_lengkap', models.CharField(default='', max_length=50)),
                ('tanggal_lahir', models.CharField(default='', max_length=50)),
                ('email', models.CharField(default='', max_length=50)),
                ('alamat', models.CharField(default='', max_length=50)),
            ],
        ),
    ]
