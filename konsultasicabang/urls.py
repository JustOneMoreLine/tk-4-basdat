from django.urls import path
from . import views
from django.conf.urls import url
from konsultasicabang.views import konsultasi, updatekonsultasi, rscabang, updaterscabang, daftarrscabang, daftarkonsultasi

app_name = 'konsultasicabang'

urlpatterns = [
    path('', views.loginregister, name='loginregister'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('administrator/', views.administrator, name='administrator'),
    path('dokter/', views.dokter, name='dokter'),
    path('pasien/', views.pasien, name='pasien'),
    path('logout/', views.logout, name='logout'),

    path('konsultasi/', views.konsultasi, name='konsultasi'),
    path('updatekonsultasi/<id>', views.updatekonsultasi, name='updatekonsultasi'),
    path('daftarkonsultasi/', views.daftarkonsultasi, name='daftarkonsultasi'),
    path('deletekonsultasi/<id>', views.deletekonsultasi, name='deletekonsultasi'),
    path('updatekonsultasi/updatekonsultasi/daftarkonsultasi/', views.daftarkonsultasi, name='daftarkonsultasi'),
    
    path('rscabang/', views.rscabang, name='rscabang'),
    path('updaterscabang/<id>', views.updaterscabang, name='updaterscabang'),
    path('daftarrscabang/', views.daftarrscabang, name='daftarrscabang'),
    path('deleterscabang/<id>', views.deleterscabang, name='deleterscabang'),
    path('updaterscabang/updaterscabang/daftarrscabang/', views.daftarrscabang, name='daftarrscabang'),

    # path('/homepagepasien/', views.hppasien, name='hppasien'),
    # path('/homepagedokter/', views.hpdokter, name='hpdokter'),
    # path('/homepageadmin/', views.hpadmin, name='hpadmin'),
]