from django.db import models

class Konsultasi(models.Model):
    no_rekam_medis = models.CharField(max_length=50, default="")
    tanggal = models.CharField(max_length=30, default="")
    id_transaksi = models.CharField(max_length=50, default="")

    id_konsultasi = models.CharField(max_length=50, default="")
    biaya = models.CharField(max_length=50, default="0")
    status = models.CharField(max_length=50, default="booked")


class RSCabang(models.Model):
    kode_rs_cabang = models.CharField(max_length=50, default="")
    nama = models.CharField(max_length=50, default="")
    tanggal_pendirian = models.CharField(max_length=50, default="")
    jalan = models.CharField(max_length=50, default="")
    nomor = models.CharField(max_length=50, default="")
    kota = models.CharField(max_length=50, default="")

class Administrator(models.Model):
    username = models.CharField(max_length=50, default="")
    password = models.CharField(max_length=50, default="")
    nomor_identitas = models.CharField(max_length=50, default="")
    nama_lengkap = models.CharField(max_length=50, default="")
    tanggal_lahir = models.CharField(max_length=50, default="")
    email = models.CharField(max_length=50, default="")
    alamat = models.CharField(max_length=50, default="")


class Dokter(models.Model):
    username = models.CharField(max_length=50, default="")
    password = models.CharField(max_length=50, default="")
    nomor_identitas = models.CharField(max_length=50, default="")
    nama_lengkap = models.CharField(max_length=50, default="")
    tanggal_lahir = models.CharField(max_length=50, default="")
    email = models.CharField(max_length=50, default="")
    alamat = models.CharField(max_length=50, default="")
    no_sip = models.CharField(max_length=50, default="")
    spesialisasi = models.CharField(max_length=50, default="")

class Pasien(models.Model):
    username = models.CharField(max_length=50, default="")
    password = models.CharField(max_length=50, default="")
    nomor_identitas = models.CharField(max_length=50, default="")
    nama_lengkap = models.CharField(max_length=50, default="")
    tanggal_lahir = models.CharField(max_length=50, default="")
    email = models.CharField(max_length=50, default="")
    alamat = models.CharField(max_length=50, default="")
    alergi = models.CharField(max_length=50, default="")

class Login(models.Model):
    email = models.CharField(max_length=50, default="")
    password = models.CharField(max_length=50, default="")
    
