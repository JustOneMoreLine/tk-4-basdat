from django import forms
from django.forms import widgets
from .models import Konsultasi, RSCabang, Administrator, Dokter, Pasien, Login
import datetime

class KonsultasiForm(forms.ModelForm):
    no_rekam_medis_pasien = forms.CharField(required=True)
    tanggal = forms.DateField(initial=datetime.date.today)
    id_transaksi = forms.CharField(required=True)

    class Meta:
        model = Konsultasi
        fields = {
            'no_rekam_medis_pasien',
            'tanggal',
            'id_transaksi'
        }
      

class UpdateKonsultasiForm(forms.ModelForm):
    tanggal = forms.DateField(initial=datetime.date.today, required=False)
    status = forms.CharField(required=False)
    class Meta:
            model = Konsultasi
            fields = {
                'tanggal',
                'status'
            } 
        
class RSCabangForm(forms.ModelForm):
        kode_rs = forms.CharField(required=True)
        nama = forms.CharField(required=True)
        tanggal_pendirian = forms.CharField(required=True)
        jalan = forms.CharField(required=False)
        nomor = forms.CharField(required=False)
        kota = forms.CharField(required=True)
        
        class Meta:
            model = RSCabang
            fields = {
                'kode_rs',
                'nama',
                'tanggal_pendirian',
                'jalan',
                'nomor',
                'kota' 
            } 

class UpdateRSCabangForm(forms.ModelForm):
        nama = forms.CharField(required=False)
        tanggal_pendirian = forms.CharField(required=False)
        jalan = forms.CharField(required=False)
        nomor = forms.CharField(required=False)
        kota = forms.CharField(required=False)

        class Meta:
            model = RSCabang
            fields = {
                'nama',
                'tanggal_pendirian',
                'jalan',
                'nomor',
                'kota' 
            }

class AdministratorForm(forms.ModelForm):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True)
    nomor_identitas = forms.CharField(required=True)
    nama_lengkap = forms.CharField(required=True)
    tanggal_lahir = forms.CharField(required=True)
    email = forms.CharField(required=True)
    alamat = forms.CharField(required=True)
    
    class Meta:
            model = Administrator
            fields = {
                'username',
                'password',
                'nomor_identitas',
                'nama_lengkap',
                'tanggal_lahir',
                'email',
                'alamat' 
            }

class DokterForm(forms.ModelForm):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True)
    nomor_identitas = forms.CharField(required=True)
    nama_lengkap = forms.CharField(required=True)
    tanggal_lahir = forms.CharField(required=True)
    email = forms.CharField(required=True)
    alamat = forms.CharField(required=True)
    no_SIP = forms.CharField(required=True)
    spesialisasi = forms.CharField(required=True)

    class Meta:
            model = Dokter
            fields = {
                'username',
                'password',
                'nomor_identitas',
                'nama_lengkap',
                'tanggal_lahir',
                'email',
                'alamat',
                'no_SIP',
                'spesialisasi' 
            }

class PasienForm(forms.ModelForm):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True)
    nomor_identitas = forms.CharField(required=True)
    nama_lengkap = forms.CharField(required=True)
    tanggal_lahir = forms.CharField(required=True)
    email = forms.CharField(required=True)
    alamat = forms.CharField(required=True)
    alergi = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
            model = Pasien
            fields = {
                'username',
                'password',
                'nomor_identitas',
                'nama_lengkap',
                'tanggal_lahir',
                'email',
                'alamat',
                'alergi'
            }

class LoginForm(forms.ModelForm):
    email = forms.CharField(required=True)
    password = forms.CharField(required=True) 
