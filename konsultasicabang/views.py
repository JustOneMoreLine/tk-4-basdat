from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from .models import Konsultasi, RSCabang, Administrator, Dokter, Pasien
from . import forms
from .forms import KonsultasiForm, RSCabangForm, UpdateKonsultasiForm, UpdateRSCabangForm, AdministratorForm, DokterForm, PasienForm, LoginForm
from django.contrib.auth import authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from django.contrib import messages

from django.db import connection
from collections import namedtuple

import random
# Create your views here.

def namedtuplefetchall(cursor):
    # "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def konsultasi(request):
    if (request.method == 'GET'):
        cursor1 = connection.cursor()
        cursor2 = connection.cursor()
        cursor1.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor2.execute('SET SEARCH_PATH TO MEDIKAGO;')
        choose_NRM = 'SELECT no_rekam_medis_pasien FROM SESI_KONSULTASI;'
        choose_IDT = 'SELECT id_transaksi FROM SESI_KONSULTASI;'

        cursor1.execute(choose_NRM)
        cursor2.execute(choose_IDT)
        data1 = namedtuplefetchall(cursor1)
        data2 = namedtuplefetchall(cursor2)

        form = KonsultasiForm(request.POST)
        listform = Konsultasi.objects.all()
        context = {
            'form' : form,
            'listform' : listform,
            'data1' : data1,
            'data2' : data2,
        }
        return render(request, 'konsultasi.html', context)

    elif (request.method == 'POST'):
        cursor_post = connection.cursor()
        no_rekam_medis_pasien = request.POST["no_rekam_medis_pasien"]
        tanggal = request.POST.get('tanggal')
        biaya = 0
        status = "Booked"
        id_transaksi = request.POST["id_transaksi"]

        cursor_post.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_post.execute("SELECT id_konsultasi FROM SESI_KONSULTASI ORDER BY id_konsultasi LIMIT 1;")
        temp_id_konsultasi = cursor_post.fetchone()[0]
        id_konsultasi = int(temp_id_konsultasi) + 1

        cursor_post.execute("SELECT COUNT(*) FROM SESI_KONSULTASI WHERE id_konsultasi ='" + str(id_konsultasi) + "'")
        if_existing = cursor_post.fetchone()[0]

        while (if_existing != 0):
            id_konsultasi = id_konsultasi + 1
            cursor_post.execute("SELECT COUNT(*) FROM SESI_KONSULTASI WHERE id_konsultasi ='" + str(id_konsultasi) + "'")
            if_existing = cursor_post.fetchone()[0]

        insertion = "INSERT INTO SESI_KONSULTASI VALUES(%s, %s, %s, %s, %s, %s);"
        cursor_post.execute(insertion, (str(id_konsultasi), no_rekam_medis_pasien, tanggal, str(biaya), status, id_transaksi))

        return redirect('/daftarkonsultasi/')
    
    return redirect('/login/')

def rscabang(request):
    if request.method == 'GET':
        form = RSCabangForm(request.POST)
        listform2 = RSCabang.objects.all()
        context = {
            'form' : form,
            'listform' : listform2,
        }
        return render(request, 'rscabang.html', context)
    
    elif request.method == 'POST':
        cursor_post = connection.cursor()
        kode_rs = request.POST.get('kode_rs')
        nama = request.POST["nama"]
        tanggal_pendirian = request.POST["tanggal_pendirian"]
        jalan = request.POST["jalan"]
        nomor = request.POST["nomor"]
        kota = request.POST["kota"]

        insertion = "INSERT INTO RS_CABANG VALUES(%s, %s, %s, %s, %s, %s);"
        cursor_post.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_post.execute(insertion, (kode_rs, nama, tanggal_pendirian, jalan, str(nomor), kota))

        return redirect('/daftarrscabang/')

    return redirect('/login/')

def updatekonsultasi(request, id):
    if request.method == 'POST':
        tanggal = request.POST.get('tanggal')
        status = request.POST.get('status')
        
        data = "UPDATE SESI_KONSULTASI SET tanggal=%s, status=%s WHERE id_konsultasi = '" + id + "'" + ";"
        cursor_update = connection.cursor()
        cursor_update.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_update.execute(data, (tanggal, status))
        
        return redirect('/daftarkonsultasi/')
    
    elif request.method == 'GET':
        form = UpdateKonsultasiForm(request.POST)
        listform = Konsultasi.objects.all()

        NRM = "SELECT no_rekam_medis_pasien FROM SESI_KONSULTASI WHERE id_konsultasi = '" + id + "'" + ";"
        biaya = "SELECT biaya FROM SESI_KONSULTASI WHERE id_konsultasi = '" + id + "'" + ";"
        IDT = "SELECT id_transaksi FROM SESI_KONSULTASI WHERE id_konsultasi = '" + id + "'" + ";"

        cursor_NRM = connection.cursor()
        cursor_biaya = connection.cursor()
        cursor_IDT = connection.cursor()

        cursor_NRM.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_biaya.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_IDT.execute('SET SEARCH_PATH TO MEDIKAGO;')

        cursor_NRM.execute(NRM)
        cursor_biaya.execute(biaya)
        cursor_IDT.execute(IDT)

        data_NRM = cursor_NRM.fetchone()[0]
        data_biaya = cursor_biaya.fetchone()[0]
        data_IDT = cursor_IDT.fetchone()[0]
        

        context = {
            'form' : form,
            'listform' : listform,
            'id_konsultasi': id,
            'data_NRM': data_NRM,
            'data_biaya': data_biaya,
            'data_IDT': data_IDT
        }

    return render(request, 'updatekonsultasi.html', context)

def updaterscabang(request, id):
    if request.method == 'POST':
        nama = request.POST.get('nama')
        tanggal_pendirian = request.POST.get('tanggal_pendirian')
        jalan = request.POST.get('jalan')
        nomor = request.POST.get('nomor')
        kota = request.POST.get('kota')
        
        data2 = "UPDATE RS_CABANG SET nama=%s, tanggal_pendirian=%s, jalan=%s, nomor=%s, kota=%s WHERE kode_rs = '" + id + "'" + ";"
        cursor_update = connection.cursor()
        cursor_update.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_update.execute(data2, (nama, tanggal_pendirian, jalan, str(nomor), kota))
    
        return redirect('/daftarrscabang/')

    elif request.method == 'GET':
        form = UpdateRSCabangForm(request.POST)
        listform = RSCabang.objects.all()

        RSC = "SELECT kode_rs FROM RS_CABANG WHERE kode_rs = '" + id + "'" + ";"
        cursor_RSC = connection.cursor()
        cursor_RSC.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_RSC.execute(RSC)
        data_RSC = cursor_RSC.fetchone()[0]

        context = {
            'form' : form,
            'listform' : listform,
            'kode_rs':id
        }        

    return render(request, 'updaterscabang.html', context)

def daftarkonsultasi(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor.execute('SELECT * FROM SESI_KONSULTASI;')
    data = namedtuplefetchall(cursor)
    return render(request, 'konsultasi2.html', {'data':data})

def daftarrscabang(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor.execute('SELECT * FROM RS_CABANG;')
    data = namedtuplefetchall(cursor)
    return render(request, 'rscabang2.html', {'data':data})

def deletekonsultasi(request, id):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor.execute("DELETE FROM SESI_KONSULTASI WHERE id_konsultasi='" + id + "'" + ";")
    return redirect('/daftarkonsultasi/')


def deleterscabang(request, id):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor.execute("DELETE FROM RS_CABANG WHERE kode_rs= '" + id + "'" + ";")
    return redirect('/daftarrscabang/')

def loginregister(request):
    return render(request, 'loginregister.html')

def login(request): 
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        
        cursor_login = connection.cursor()
        cursor_login.execute("SELECT email, password FROM MEDIKAGO.PENGGUNA WHERE email= '" + email + "'" + ";")
        user = cursor_login.fetchone()

        if user is not None:
             if password == user[1]:
                request.session['username'] = username
                cursor_pasien = connection.cursor()
                cursor_pasien.execute("SELECT * FROM MEDIKAGO.PASIEN WHERE email= '" + email + "'" + ";")
                role_pasien = namedtuplefetchall(cursor_pasien)

                cursor_dokter = connection.cursor()
                cursor_dokter.execute("SELECT * FROM MEDIKAGO.DOKTER WHERE email= '" + email + "'" + ";")
                role_dokter = namedtuplefetchall(cursor_dokter)

                cursor_administrator = connection.cursor()
                cursor_administrator.execute("SELECT * FROM MEDIKAGO.ADMINISTRATOR WHERE email= '" + email + "'" + ";")
                role_administrator = namedtuplefetchall(cursor_administrator)
                
                if len(role_pasien) != 0:
                    request.session['role'] = 'pasien'
                elif len(role_dokter) != 0:
                    request.session['role'] = 'dokter'
                elif len(role_administrator) != 0:
                    request.session['role'] = 'administrator'
                return redirect('/login/homepage/')
    
    form = LoginForm(request.POST)
   
    context = {
        'form' : form,
    }

    return render(request, 'login.html', context)

def register(request):
    return render(request, 'register.html')

def administrator(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        nomor_identitas = request.POST.get('nomor_identitas')
        nama_lengkap = request.POST.get('nama_lengkap')
        tanggal_lahir = request.POST.get('tanggal_lahir')
        email = request.POST.get('email')
        alamat = request.POST.get('alamat')
        role = 'admin'

        cursor_pengguna = connection.cursor()
        cursor_pengguna.execute("SELECT COUNT(*) FROM MEDIKAGO.PENGGUNA WHERE email = '" + email + "'" + ";")
        if_existing_pengguna = cursor_pengguna.fetchone()

        cursor_admin = connection.cursor()
        cursor_admin.execute("SELECT COUNT(*) FROM MEDIKAGO.ADMINISTRATOR WHERE username = '" + username + "'" + ";")
        if_existing_admin = cursor_admin.fetchone()

        if if_existing_pengguna[0] == 0 and if_existing_admin[0] == 0:
            insertion = "INSERT INTO MEDIKAGO.PENGGUNA VALUES(%s, %s, %s, %s, %s, %s, %s);"
            cursor_pengguna.execute(insertion, (email, username, password, nama_lengkap, nomor_identitas, tanggal_lahir, alamat))

            cursor_admin.execute("SELECT kode_rs FROM MEDIKAGO.ADMINISTRATOR;")
            list_koders = namedtuplefetchall(cursor_admin)
            kode_rs = random.choice(list_koders)

            insertion2 = "INSERT INTO MEDIKAGO.ADMINISTRATOR VALUES(%s, %s, %s);"
            cursor_pengguna.execute(insertion2, (nomor_identitas, username, str(kode_rs)))
        
        return render(request, 'hpadministrator.html')

    elif request.method == 'GET':
        form = AdministratorForm(request.POST)
        listform = Administrator.objects.all()
        context = {
            'form' : form,
            'listform' : listform,
        }
        return render(request, 'administrator.html', context)


    return render(request, 'hpadministrator.html', context)

def dokter(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        nomor_identitas = request.POST.get('nomor_identitas')
        nama_lengkap = request.POST.get('nama_lengkap')
        tanggal_lahir = request.POST.get('tanggal_lahir')
        email = request.POST.get('email')
        alamat = request.POST.get('alamat')
        no_SIP = request.POST.get('no_SIP')
        spesialisasi = request.POST.get('spesialisasi')
        role = 'dokter'

        cursor_pengguna = connection.cursor()
        cursor_pengguna.execute("SELECT COUNT(*) FROM MEDIKAGO.PENGGUNA WHERE email = '" + email + "'" + ";")
        if_existing_pengguna = cursor_pengguna.fetchone()

        cursor_dokter = connection.cursor()
        cursor_dokter.execute("SELECT COUNT(*) FROM MEDIKAGO.DOKTER WHERE no_sip = '" + no_SIP + "'" + ";")
        if_existing_dokter = cursor_dokter.fetchone()

        if if_existing_pengguna[0] == 0 and if_existing_admin[0] == 0:
            insertion = "INSERT INTO MEDIKAGO.PENGGUNA VALUES(%s, %s, %s, %s, %s, %s);"
            cursor_pengguna.execute(insertion, (email, username, password, nama_lengkap, nomor_identitas, tanggal_lahir, alamat))

            cursor_dokter.execute("SELECT id_dokter FROM MEDIKAGO.DOKTER ORDER BY id_dokter LIMIT 1;")
            temp_id_dokter = cursor_dokter.fetchone()[0]
            id_dokter = int(temp_id_dokter) + 1

            insertion2 = "INSERT INTO MEDIKAGO.DOKTER VALUES(%s, %s, %s, %s);"
            cursor_pengguna.execute(insertion2, (str(id_dokter), username, no_SIP, spesialisasi))
        
        return render(request, 'hpdokter.html')

    elif request.method == 'GET':
        form = DokterForm(request.POST)
        listform = Dokter.objects.all()
        context = {
            'form' : form,
            'listform' : listform,
        }
        return render(request, 'dokter.html', context)


    return render(request, 'hpdokter.html', context)

def pasien(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        nomor_identitas = request.POST.get('nomor_identitas')
        nama_lengkap = request.POST.get('nama_lengkap')
        tanggal_lahir = request.POST.get('tanggal_lahir')
        email = request.POST.get('email')
        alamat = request.POST.get('alamat')
        alergi = request.POST.get('alergi')
        role = 'pasien'

        cursor_pengguna = connection.cursor()
        cursor_pengguna.execute("SELECT COUNT(*) FROM MEDIKAGO.PENGGUNA WHERE email = '" + email + "'" + ";")
        if_existing_pengguna = cursor_pengguna.fetchone()

        cursor_admin = connection.cursor()
        cursor_admin.execute("SELECT COUNT(*) FROM MEDIKAGO.PASIEN WHERE username = '" + email + "'" + ";")
        if_existing_admin = cursor_admin.fetchone()

        if if_existing_pengguna[0] == 0 and if_existing_admin[0] == 0:
            insertion = "INSERT INTO MEDIKAGO.PENGGUNA VALUES(%s, %s, %s, %s, %s, %s);"
            cursor_pengguna.execute(insertion, (email, username, password, nama_lengkap, nomor_identitas, tanggal_lahir, alamat))

            cursor_pasien.execute("SELECT no_rekam_medis FROM MEDIKAGO.PASIEN ORDER BY no_rekam_medis LIMIT 1;")
            temp_NRM = cursor_pasien.fetchone()[0]
            no_rekam_medis = int(temp_NRM) + 1

            cursor_pasien.execute("SELECT nama FROM MEDIKAGO.ASURANSI;")
            list_asuransi = namedtuplefetchall(cursor_pasien)
            asuransi = random.choice(list_asuransi)

            insertion2 = "INSERT INTO MEDIKAGO.PASIEN VALUES(%s, %s, %s);"
            cursor_pengguna.execute(insertion2, (str(no_rekam_medis), username, asuransi))
        
        return render(request, 'hppasien.html')

    elif request.method == 'GET':
        form = PasienForm(request.POST)
        listform = Pasien.objects.all()
        context = {
            'form' : form,
            'listform' : listform,
        }
        return render(request, 'pasien.html', context)


    return render(request, 'hppasien.html', context)
    

def logout (request):
    request.session.flush() 
    auth_logout(request)
    return redirect ('/')

