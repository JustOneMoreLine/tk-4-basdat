from django import forms
from .models import Tindakan, TindakanPoli

class TindakanPoliForm(forms.ModelForm):

    id_poliklinik = forms.CharField(required=True)
    nama_tindakan = forms.CharField(required=True)
    deskripsi = forms.CharField(required=False)
    tarif = forms.IntegerField(required=True)

    class Meta:
        model = TindakanPoli
        fields = {
            'id_poliklinik',
            'nama_tindakan',
            'deskripsi',
            'tarif'
        }
      
class UpdateTindakanPoliForm(forms.ModelForm):
    id_poliklinik = forms.CharField(required=False)
    nama_tindakan = forms.CharField(required=False)
    deskripsi = forms.CharField(required=False)
    tarif = forms.CharField(required=False)
    class Meta:
            model = TindakanPoli
            fields = {
                'id_poliklinik',
                'nama_tindakan',
                'deskripsi',
                'tarif'
            } 
        
class TindakanForm(forms.ModelForm):
        id_konsultasi = forms.CharField(required=True)
        id_transaksi = forms.CharField(required=True)
        catatan = forms.CharField(required=False)
        daftar_id_tpoli1 = forms.CharField(required=True)
        daftar_id_tpoli2 = forms.CharField(required=False)
        daftar_id_tpoli3 = forms.CharField(required=False)
        daftar_id_tpoli4 = forms.CharField(required=False)
        daftar_id_tpoli5 = forms.CharField(required=False)

        class Meta:
            model = Tindakan
            fields = {
                'id_konsultasi',
                'id_transaksi',
                'catatan',
                'daftar_id_tpoli1',
                'daftar_id_tpoli2',
                'daftar_id_tpoli3',
                'daftar_id_tpoli4',
                'daftar_id_tpoli5'
            } 

class UpdateTindakanForm(forms.ModelForm):
        catatan = forms.CharField(required=False)

        class Meta:
            model = Tindakan
            fields = {
                'catatan'
            }