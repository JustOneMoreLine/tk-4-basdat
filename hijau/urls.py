from django.urls import path
from . import views
from django.conf.urls import url
from django.conf.urls import include
from hijau.views import tindakanpoli, updatetpoli, tindakan, updatetindakan, daftartindakan, daftartpoli

app_name = 'hijau'

urlpatterns = [
    #path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),

    path('tindakanpoli/', views.tindakanpoli, name='tindakanpoli'),
    path('updatetpoli/<id>', views.updatetpoli, name='updatetpoli'),
    path('daftartpoli/', views.daftartpoli, name='daftartpoli'),
    path('deletekonsultasi/<id>', views.deletetpoli, name='deletetpoli'),
    path('updatetpoli/updatetpoli/daftartpoli/', views.daftartpoli, name='daftartpoli'),

    path('tindakan/', views.tindakan, name='tindakan'),
    path('updatetindakan/<id>', views.updatetindakan, name='updatetindakan'),
    path('daftartindakan/', views.daftartindakan, name='daftartindakan'),
    path('deletetindakan/<id>', views.delete_tindakan, name='deletetindakan'),
    path('updatetindakan/updatetindakan/daftartindakan/', views.daftartindakan, name='daftartindakan'),
]