from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from django.http import HttpResponse
from .models import Tindakan, TindakanPoli
from . import forms
from .forms import TindakanForm, TindakanPoliForm, UpdateTindakanForm, UpdateTindakanPoliForm
from django.contrib.auth import authenticate
from django.contrib import messages

from django.db import connection
from collections import namedtuple

import random

def namedtuplefetchall(cursor):
    # "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# Create your views here.

def home(request):
    return render(request, 'home.html')

def profile(request):
    return render(request, 'profile.html')

def tindakanpoli(request):
    if request.method == 'GET':
        cursorIDP = connection.cursor()
        choose_IDP = 'SELECT id_poliklinik FROM MEDIKAGO.TINDAKAN_POLI;'
        cursorIDP.execute(choose_IDP)
        dataIDP = namedtuplefetchall(cursor)

        form = TindakanPoliForm(request.POST)
        listform = TindakanPoli.objects.all()
        context = {
            'form' : form,
            'listform' : listform,
            'data' : dataIDP,
        }
        return render(request, 'Ctpoli.html', context)
    elif request.method == 'POST':
        cursor_post = connection.cursor()
        id_poliklinik = request.POST["id_poliklinik"]
        nama_tindakan = request.POST["nama_tindakan"]
        deskripsi = request.POST["deskripsi"]
        tarif = request.POST["tarif"]
        
        cursor_post.execute("SELECT id_tindakan_poli FROM MEDIKAGO.TINDAKAN_POLI ORDER BY id_tindakan_poli LIMIT 1;")
        temp_id_tindakan_poli = cursor_post.fetchone()[0]
        id_tindakan_poli = int(temp_id_tindakan_poli) + 1

        cursor_post.execute("SELECT COUNT(*) FROM MEDIKAGO.TINDAKAN_POLI WHERE id_tindakan_poli ='" + str(id_tindakan_poli) + "'")
        if_existing = cursor_post.fetchone()[0]

        while (if_existing != 0):
            id_tindakan_poli = id_tindakan_poli + 1
            cursor_post.execute("SELECT COUNT(*) FROM MEDIKAGO.TINDAKAN_POLI WHERE id_tindakan_poli ='" + str(id_tindakan_poli) + "'")
            if_existing = cursor_post.fetchone()[0]

        insertion = "INSERT INTO MEDIKAGO.TINDAKAN_POLI VALUES(%s, %s, %s, %s, %s);"
        cursor_post.execute(insertion, (str(id_tindakan_poli), id_poliklinik, nama_tindakan, str(tarif), deskripsi))

        return redirect('/daftartpoli/')

def updatetpoli(request, id):
    if request.method == 'POST':
        id_poliklinik = request.POST["id_poliklinik"]
        nama_tindakan = request.POST["nama_tindakan"]
        deskripsi = request.POST["deskripsi"]
        tarif = request.POST["tarif"]
        data = "UPDATE MEDIKAGO.SESI_KONSULTASI SET id_poliklinik=%s, nama=%s, deskripsi=%s, tarif=%s, WHERE id_tindakan_poli = '" + id + "'" + ";"
        cursor_update = connection.cursor()
        cursor_update.execute(data, (id_poliklinik, nama, deskripsi, tarif))
        
        return redirect('/daftartpoli/')
    
    elif request.method == 'GET':
        form = UpdateTindakanPoliForm(request.POST)
        listform = TindakanPoli.objects.all()

        context = {
            'form' : form,
            'listform' : listform,
            'id_tindakan_poli': id
        }

    return render(request, 'Utpoli.html', context)

def daftartpoli(request):
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM MEDIKAGO.TINDAKAN_POLI;')
    data = namedtuplefetchall(cursor)
    return render(request, 'Rtpoli.html', {'data':data})

def deletetpoli(request, id):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM MEDIKAGO.TINDAKAN_POLI WHERE id_tindakan_poli='" + id + "'" + ";")
    return redirect('/daftartpoli/')

def tindakan(request):
    if (request.method == 'GET'):
        cursorIDK = connection.cursor()
        cursorIDT = connection.cursor()
        cursor1 = connection.cursor()
        cursor2 = connection.cursor()
        cursor3 = connection.cursor()
        cursor4 = connection.cursor()
        cursor5 = connection.cursor()
        choose_IDK = 'SELECT id_konsultasi FROM MEDIKAGO.TINDAKAN;'
        choose_IDT = 'SELECT id_transaksi FROM MEDIKAGO.TINDAKAN;'
        choose_1 = 'SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.TINDAKAN.id_konsultasi = MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi; '
        choose_2 = 'SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.TINDAKAN.id_konsultasi = MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi; '
        choose_3 = 'SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.TINDAKAN.id_konsultasi = MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi; '
        choose_4 = 'SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.TINDAKAN.id_konsultasi = MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi; '
        choose_5 = 'SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.TINDAKAN.id_konsultasi = MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi; '
        cursorIDK.execute(choose_IDK)
        cursorIDT.execute(choose_IDT)
        cursor1.execute(choose_1)
        cursor2.execute(choose_2)
        cursor3.execute(choose_3)
        cursor4.execute(choose_4)
        cursor5.execute(choose_5)

        dataIDK = namedtuplefetchall(cursorIDK)
        dataIDT = namedtuplefetchall(cursorIDT)
        data1 = namedtuplefetchall(cursor1)
        data2 = namedtuplefetchall(cursor2)
        data3 = namedtuplefetchall(cursor3)
        data4 = namedtuplefetchall(cursor4)
        data5 = namedtuplefetchall(cursor5)

        form = KonsultasiForm(request.POST)
        listform = Konsultasi.objects.all()
        context = {
            'form' : form,
            'listform' : listform,
            'dataIDK' : dataIDK,
            'dataIDT' : dataIDT,
            'data1' : data1,
            'data2' : data2,
            'data3' : data3,
            'data4' : data4,
            'data5' : data5,
        }
        return render(request, 'Ctindakan.html', context)

    elif (request.method == 'POST'):
        id_konsultasi = request.POST["id_konsultasi"]
        id_transaksi = request.POST["id_transaksi"]
        catatan = request.POST["catatan"]
        daftar_id_tpoli1 = request.POST["daftar_id_tpoli1"]
        daftar_id_tpoli2 = request.POST["daftar_id_tpoli2"]
        daftar_id_tpoli3 = request.POST["daftar_id_tpoli3"]
        daftar_id_tpoli4 = request.POST["daftar_id_tpoli4"]
        daftar_id_tpoli5 = request.POST["daftar_id_tpoli5"]

        cursor_post.execute("SELECT biaya FROM MEDIKAGO.TINDAKAN WHERE id_konsultasi = '" + str(id_konsultasi) + "'")
        biaya = cursor_post.fetchone()[0]

        cursor_post.execute("SELECT no_urut FROM MEDIKAGO.TINDAKAN ORDER BY no_urut LIMIT 1;")
        temp_no_urut = cursor_post.fetchone()[0]
        no_urut  = int(no_urut) + 1

        cursor_post.execute("SELECT COUNT(*) FROM MEDIKAGO.SESI_KONSULTASI WHERE no_urut  ='" + str(no_urut) + "'")
        if_existing = cursor_post.fetchone()[0]

        while (if_existing != 0):
            no_urut = no_urut  + 1
            cursor_post.execute("SELECT COUNT(*) FROM MEDIKAGO.SESI_KONSULTASI WHERE no_urut  ='" + str(no_urut) + "'")
            if_existing = cursor_post.fetchone()[0]

        insertion = "INSERT INTO MEDIKAGO.TINDAKAN VALUES(%s, %s, %s, %s, %s);"
        cursor_post.execute(insertion, (str(id_konsultasi), no_urut, biaya, catatan, id_transaksi))

        return redirect('/daftartindakan/')

def updatetindakan(request, id):
    if request.method == 'POST':
        catatan = request.POST.get('catatan')
        data = "UPDATE MEDIKAGO.TINDAKAN SET catatan=%s WHERE id_konsultasi = '" + id + "'" + ";"
        cursor_update = connection.cursor()
        cursor_update.execute(data, (catatan))
        return redirect('/daftartindakan/')

    elif request.method == 'GET':
        form = UpdateTindakanForm(request.POST)
        listform = Tindakan.objects.all()

        IDK = "SELECT id_konsultasi FROM MEDIKAGO.TINDAKAN WHERE id_konsultasi = '" + id + "'" + ";"
        IDT = "SELECT id_transaksi FROM MEDIKAGO.TINDAKAN WHERE id_konsultasi = '" + id + "'" + ";"
        biaya = "SELECT biaya FROM MEDIKAGO.TINDAKAN WHERE id_konsultasi = '" + id + "'" + ";"
        c1 = "SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi ='" + id + "'" + " ; "
        c2 = "SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi ='" + id + "'" + " ; "
        c3 = "SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi ='" + id + "'" + " ; "
        c4 = "SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi ='" + id + "'" + " ; "
        c5 = "SELECT id_tindakan_poli FROM MEDIKAGO.DAFTAR_TINDAKAN WHERE MEDIKAGO.DAFTAR_TINDAKAN.id_konsultasi ='" + id + "'" + " ; "

        cursorIDK = connection.cursor()
        cursorIDT = connection.cursor()
        cursorbiaya = connection.cursor()
        cursor1 = connection.cursor()
        cursor2 = connection.cursor()
        cursor3 = connection.cursor()
        cursor4 = connection.cursor()
        cursor5 = connection.cursor()

        cursorIDK.execute(IDK)
        cursorIDT.execute(IDT)
        cursorbiaya.execute(biaya)
        cursor1.execute(c1)
        cursor2.execute(c2)
        cursor3.execute(c3)
        cursor4.execute(c4)
        cursor5.execute(c5)

        dataIDK = cursorIDK.fetchone()[0]
        dataIDT = cursorIDT.fetchone()[0]
        databiaya = cursorbiaya.fetchone()[0]
        data1 = cursor1.fetchone()[0]
        data2 = cursor2.fetchone()[0]
        data3 = cursor3.fetchone()[0]
        data4 = cursor4.fetchone()[0]
        data5 = cursor5.fetchone()[0]
        

        context = {
            'form' : form,
            'listform' : listform,
            'id_konsultasi': id,
            'dataIDK': dataIDK,
            'dataIDT': dataIDT,
            'databiaya': databiaya,
            'data1': data1,
            'data2': data2,
            'data3': data3,
            'data4': data4,
            'data5': data5
        }

    return render(request, 'Utindakan.html', context)

def daftartindakan(request):
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM MEDIKAGO.TINDAKAN;')
    data = namedtuplefetchall(cursor)
    return render(request, 'Rtindakan.html', {'data':data})

def delete_tindakan(request, id): 
    cursor = connection.cursor()
    cursor.execute("DELETE FROM MEDIKAGO.TINDAKAN WHERE id_konsultasi= '" + id + "'" + ";")
    return redirect('/daftartindakan/')

