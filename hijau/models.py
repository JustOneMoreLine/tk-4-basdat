from django.db import models

class TindakanPoli(models.Model):
    id_poliklinik = models.CharField(max_length=50, default="")
    nama_tindakan = models.CharField(max_length=50, default="")
    deskripsi = models.CharField(max_length=200, default="")

    tarif = models.IntegerField(default=0)
    id_tindakan_poli = models.CharField(max_length=50, default="")

class Tindakan(models.Model):
    id_konsultasi = models.CharField(max_length=50, default="")
    id_transaksi = models.CharField(max_length=50, default="")
    catatan = models.CharField(max_length=200, default="")
    daftar_id_tpoli1 = models.CharField(max_length=50, default="")
    daftar_id_tpoli2 = models.CharField(max_length=50, default="")
    daftar_id_tpoli3 = models.CharField(max_length=50, default="")
    daftar_id_tpoli4 = models.CharField(max_length=50, default="")
    daftar_id_tpoli5 = models.CharField(max_length=50, default="")

    biaya = models.IntegerField(default=0)
    no_urut = models.CharField(max_length=50, default="")
