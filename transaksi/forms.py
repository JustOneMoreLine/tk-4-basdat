from django import forms
from django.forms import widgets
from .models import Transaksi
import datetime

class TransaksiForm(forms.ModelForm):
    no_rekam_medis_pasien = forms.CharField(required=True)
    
    class Meta:
        model = Transaksi
        fields = {
            'no_rekam_medis_pasien'
        }
      

class UpdateTransaksiForm(forms.ModelForm):
    tanggal = forms.DateField(initial=datetime.date.today, required=False)
    status = forms.CharField(required=False)
    waktu_bayar = forms.DateField(initial=datetime.date.today, required=False)
    class Meta:
            model = Transaksi
            fields = {
                'tanggal',
                'status',
                'waktu_bayar'
            } 
        