from django.db import models

class Transaksi(models.Model):
	id_transaksi = models.CharField(max_length=50, default="")
	tanggal = models.CharField(max_length=30, default="")
	status = models.CharField(max_length=50, default="created")
	biaya = models.CharField(max_length=50, default="0")
	waktu_bayar = models.CharField(max_length=30, default="")
	no_rekam_medis = models.CharField(max_length=50, default="")