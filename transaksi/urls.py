from django.urls import include, path
from . import views
from django.shortcuts import render
from django.conf.urls import url
from transaksi.views import transaksi, updatetransaksi, deletetransaksi, daftartransaksi

app_name = 'transaksi'
urlpatterns = [
    # path('', views.daftarTransaksi, name='daftartransaksi'),
    # path('rud-transaksi/', views.rudTransaksi, name='rudtransaksi'),

    path('transaksi/', views.transaksi, name='transaksi'),
    path('updatetransaksi/<id>', views.updatetransaksi, name='updatetransaksi'),
    path('daftartransaksi/', views.daftartransaksi, name='daftartransaksi'),
    path('deletetransaksi/<id>', views.deletetransaksi, name='deletetransaksi'),
    path('updatetransaksi/updatetransaksi/daftartransaksi/', views.daftartransaksi, name='daftartransaksi'),
]

