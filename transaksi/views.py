from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from .models import Transaksi
from . import forms
from .forms import TransaksiForm, UpdateTransaksiForm
from django.contrib.auth import authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from django.contrib import messages

from django.db import connection
from collections import namedtuple

import random


# Create your views here.

def namedtuplefetchall(cursor):
    # "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchal]

def transaksi(request):
    if (request.method == 'GET'):
        cursor1 = connection.cursor()
        # cursor1.execute('SET SEARCH_PATH TO MEDIKAGO;')
        choose_NRM = 'SELECT no_rekam_medis_pasien FROM MEDIKAGO.TRANSAKSI;'
        cursor1.execute(choose_NRM)
        data1 = namedtuplefetchall(cursor1)

        form = TransaksiForm(request.POST)
        listform = Transaksi.objects.all()
        context = {
            'form' : form,
            'listform' : listform,
            'data1' : data1,
        }
        return render(request, 'create-transaksi-admin.html', context)

    elif (request.method == 'POST'):
        cursor_post = connection.cursor()
        no_rekam_medis_pasien = request.POST["no_rekam_medis_pasien"]
        tanggal = request.POST.get('tanggal')
        biaya = 0
        status = "created"
        waktu_bayar = request.POST.get('waktu_bayar')
        id_transaksi = request.POST["id_transaksi"]

        # cursor_post.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_post.execute("SELECT id_konsultasi FROM MEDIKAGO.TRANSAKSI ORDER BY id_transaksi LIMIT 1;")
        temp_id_transaksi = cursor_post.fetchone()[0]
        id_transaksi = int(temp_id_transaksi) + 1

        cursor_post.execute("SELECT COUNT(*) FROM MEDIKAGO.TRANSAKSI WHERE id_transaksi ='" + str(id_transaksi) + "'")
        if_existing = cursor_post.fetchone()[0]

        while (if_existing != 0):
            id_transaksi = id_transaksi + 1
            cursor_post.execute("SELECT COUNT(*) FROM MEDIKAGO.TRANSAKSI WHERE id_transaksi ='" + str(id_transaksi) + "'")
            if_existing = cursor_post.fetchone()[0]

        insertion = "INSERT INTO MEDIKAGO.TRANSAKSI VALUES(%s, %s, %s, %s, %s, %s);"
        cursor_post.execute(insertion, (str(id_transaksi), tanggal, status, str(biaya), waktu_bayar, no_rekam_medis_pasien))

        return redirect('/daftartransaksi/')

def updatetransaksi(request):
    if request.method == 'POST':
        tanggal = request.POST.get('tanggal')
        status = request.POST.get('status')
        waktu_bayar = request.POST.get('waktu_bayar')
        
        if tanggal == "":
            cursor_tanggal = connection.cursor()
            # cursor_tanggal.execute('SET SEARCH_PATH TO MEDIKAGO;')
            cursor_tanggal.execute("SELECT tanggal FROM MEDIKAGO.TRANSAKSI WHERE id_transaksi ='" + id + "'" + ";")
            tanggal = cursor_tanggal.fetchone()[0]
        
        if status == "":
            cursor_status = connection.cursor()
            # cursor_status.execute('SET SEARCH_PATH TO MEDIKAGO;')
            cursor_status.execute("SELECT status FROM MEDIKAGO.TRANSAKSI WHERE id_transaksi ='" + id + "'" + ";")
            status = cursor_status.fetchone()[0]

        if waktu_bayar == "":
            cursor_waktu_bayar = connection.cursor()
            # cursor_waktu_bayar.execute('SET SEARCH_PATH TO MEDIKAGO;')
            cursor_waktu_bayar.execute("SELECT status FROM MEDIKAGO.TRANSAKSI WHERE id_transaksi ='" + id + "'" + ";")
            waktu_bayar = cursor_waktu_bayar.fetchone()[0]
        
        data = "UPDATE TRANSAKSI SET tanggal=%s, status=%s, waktu_bayar=%s WHERE id_transaksi = '" + id + "'" + ";"
        cursor_update = connection.cursor()
        # cursor_update.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor_update.execute(data, (tanggal, status, waktu_bayar))
        
        return redirect('/daftarTransaksi/')
    
    elif request.method == 'GET':
        form = UpdateTransaksiForm(request.POST)
        listform = Konsultasi.objects.all()

        # IDT = "SELECT id_transaksi FROM TRANSAKSI WHERE id_transaksi = '" + id + "'" + ";"
        # biaya = "SELECT biaya FROM TRANSAKSI WHERE id_transaksi = '" + id + "'" + ";"
        # NRM = "SELECT no_rekam_medis_pasien FROM TRANSAKSI WHERE id_transaksi = '" + id + "'" + ";"

        # cursor_NRM = connection.cursor()
        # cursor_biaya = connection.cursor()
        # cursor_IDT = connection.cursor()

        # cursor_NRM.execute('SET SEARCH_PATH TO MEDIKAGO;')
        # cursor_biaya.execute('SET SEARCH_PATH TO MEDIKAGO;')
        # cursor_IDT.execute('SET SEARCH_PATH TO MEDIKAGO;')

        # cursor_NRM.execute(NRM)
        # cursor_biaya.execute(biaya)
        # cursor_IDT.execute(IDT)

        # data_NRM = cursor_NRM.fetchone()[0]
        # data_biaya = cursor_biaya.fetchone()[0]
        # data_IDT = cursor_IDT.fetchone()[0]
        

        context = {
            'form' : form,
            'listform' : listform,
            'id_transaksi': id,
            # 'data_NRM': data_NRM,
            # 'data_biaya': data_biaya,
            # 'data_IDT': data_IDT
        }

    return render(request, 'update-transaksi.html', context)

def rudTransaksiNonAdmin(request):
    return render(request, 'RUD-transaksi.html')

def daftartransaksi(request):
    cursor = connection.cursor()
    # cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor.execute('SELECT * FROM MEDIKAGO.TRANSAKSI;')
    data = namedtuplefetchall(cursor)
    return render(request, 'RUD-transaksi-admin.html', {'data':data})

def deletetransaksi(request, id):
    cursor = connection.cursor()
    # cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor.execute("DELETE FROM MEDIKAGO.TRANSAKSI WHERE id_transaksi='" + id + "'" + ";")
    return redirect('/daftartransaksi/')
