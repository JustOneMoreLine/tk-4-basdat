from django.db import models

# Create your models here.
class Dokter_RsCabang(models.Model):
    id_dokter = id_dokter = models.CharField(max_length=50)
    kode_rs = kode_rs = models.CharField(max_length=50)

class LayananPoliklinik(models.Model):
    id_poliklinik = models.CharField(max_length=50, primary_key= True)
    nama_layanan = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=50)
    rs_cabang = models.CharField(max_length=50)

    def getId(self):
        return str(self.id_poliklinik)
    def getNama(self):
        return str(self.nama_layanan)
    def getDesc(self):
        return str(self.deskripsi)
    def getRs(self):
        return str(self.rs_cabang)

class UpdateLayananPoliklinik(models.Model):
    rs_cabang = models.CharField(max_length=50)

class UpdateJadwalLayananPoliklinik(models.Model):
    rs_cabang = models.CharField(max_length=50)

class JadwalLayananPoliklinik(models.Model):
    id_jadwal_poliklinik = models.CharField(max_length=50, default="", primary_key= True)
    waktu_mulai = models.CharField(max_length=50, default="")
    waktu_selesai = models.CharField(max_length=50, default="")
    hari = models.CharField(max_length=50, default="")
    kapasitas = models.CharField(max_length=50, default="")
    id_dokter = models.CharField(max_length=50, default="")
    id_poliklinik = models.CharField(max_length=50, default="")

    def getIdJadwalPoliklinik(self):
        return str(self.id_jadwal_poliklinik)
    def getWaktuMulai(self):
        return str(self.waktu_mulai)
    def getWaktuSelesai(self):
        return str(self.waktu_selesai)
    def getHari(self):
        return str(self.hari)
    def getKapasitas(self):
        return str(self.kapasitas)
    def getIdDokter(self):
        return str(self.id_dokter)
    def getIdPoliklinik(self):
        return str(self.id_poliklinik)