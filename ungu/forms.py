from django import forms
from .models import *
import datetime

class FormDaftarDokterRsCabang(forms.ModelForm):
    id_dokter = forms.ChoiceField(choices=
    [('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
    ('7', '7')
    ])

    kode_rs = forms.ChoiceField(choices=
    [('1001', '1001'),
    ('1002', '1002'),
    ('1003', '1003'),
    ('1004', '1004'),
    ('1005', '1005')
    ])

    class Meta:
        model = Dokter_RsCabang
        fields = {
            'id_dokter',
            'kode_rs'
            }

class FormUpdateLayananPoliklinik(forms.ModelForm):
    rs_cabang = forms.ChoiceField(choices=[
        ('200001', '20001'),
        ('200002', '20002'),
        ('200003', '20003'),
        ('200004', '20004'),
        ('200005', '20005'),
    ]
    )
    
    class Meta:
        model = UpdateLayananPoliklinik
        fields = {
            'rs_cabang'
        }

class FormUpdateJadwalLayananPoliklinik(forms.ModelForm):
    id_dokter = forms.ChoiceField(choices=[
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7')
    ])

    class Meta:
        model = UpdateJadwalLayananPoliklinik
        fields = {
            'id_dokter'
        }
