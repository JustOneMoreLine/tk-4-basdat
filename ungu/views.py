from django.shortcuts import render, redirect
from django.http import HttpResponse
from ungu.forms import *
from ungu.models import *
from django.db import connection
from collections import namedtuple


# Create your views here.

def namedtuplefetchall(cursor):
    # "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def daftardokterrs(request):
    if request.method == "GET":
        form = FormDaftarDokterRsCabang(request.GET)
        return render(request, 'daftar-dokter-rs.html', {"form" : form})
    elif request.method == "POST":
        form = FormDaftarDokterRsCabang(request.POST)
        if form.is_valid():
            id_dokter = form.cleaned_data["id_dokter"]
            kode_rs = form.cleaned_data["kode_rs"]
            cursor = connection.cursor()
            cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
            insert = 'INSERT INTO MEDIKAGO.DOKTER_RS_CABANG VALUES (%s, %s)'
            cursor.execute(insert, (id_dokter, kode_rs))
            return redirect("rudDokterRsAdmin")

def rudDokterRsAdmin(request):
    cursor1 = connection.cursor()
    cursor2 = connection.cursor()
    cursor1.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor2.execute('SET SEARCH_PATH TO MEDIKAGO;')

    kode_rs_list_com = 'SELECT kode_rs FROM DOKTER_RS_CABANG;'
    kode_rs_list_with_dok_com = 'SELECT kode_rs, id_dokter FROM DOKTER_RS_CABANG;'

    cursor1.execute(kode_rs_list_com)
    cursor2.execute(kode_rs_list_with_dok_com)

    data1 = namedtuplefetchall(cursor1)
    data2 = namedtuplefetchall(cursor2)

    listOList = {}
    for i in data1:
        if i[0] in listOList:
            continue
        else:
            listOList[i[0]] = [j[1] for j in data2 if j[0] == i[0]]

    #listOfList = { "100001" : ["1", "4"], "100002" : ["2", "7"], "100003" : ["3", "2"], "100004" : ["4", "6"], "100005" : ["5", "1"] }
    return render(request, 'RUD-dokter-rs-admin.html', {"rsList" : listOList})

def rudDokterRs(request):
    cursor1 = connection.cursor()
    cursor2 = connection.cursor()
    cursor1.execute('SET SEARCH_PATH TO MEDIKAGO;')
    cursor2.execute('SET SEARCH_PATH TO MEDIKAGO;')

    kode_rs_list_com = 'SELECT kode_rs FROM DOKTER_RS_CABANG;'
    kode_rs_list_with_dok_com = 'SELECT kode_rs, id_dokter FROM DOKTER_RS_CABANG;'

    cursor1.execute(kode_rs_list_com)
    cursor2.execute(kode_rs_list_with_dok_com)

    data1 = namedtuplefetchall(cursor1)
    data2 = namedtuplefetchall(cursor2)

    listOList = {}
    for i in data1:
        if i[0] in listOList:
            continue
        else:
            listOList[i[0]] = [j[1] for j in data2 if j[0] == i[0]]
    #listOfList = { "100001" : ["1", "4"], "100002" : ["2", "7"], "100003" : ["3", "2"], "100004" : ["4", "6"], "100005" : ["5", "1"] }
    return render(request, 'RUD-dokter-rs.html', {"rsList" : listOList})


def dokRsUpdate(request):
    if request.method == 'GET':
        idRs = request.GET['rs']
        idDoc = request.GET['doc']
        cursor1 = connection.cursor()
        cursor2 = connection.cursor()
        cursor1.execute('SET SEARCH_PATH TO MEDIKAGO;')
        cursor2.execute('SET SEARCH_PATH TO MEDIKAGO;')

        kode_rs_list_com = 'SELECT id_dokter FROM DOKTER;'
        id_dok_list_com = 'SELECT kode_rs FROM RS_CABANG;'

        cursor1.execute(kode_rs_list_com)
        cursor2.execute(id_dok_list_com)

        data1 = namedtuplefetchall(cursor1)
        data2 = namedtuplefetchall(cursor2)
        listDoctors = [i[0] for i in data1]
        listRs = [i[0] for i in data2]

        if idRs in listRs:
            listRs.remove(idRs)
        if idDoc in listDoctors:
            listDoctors.remove(idDoc)
        return render(request, 'update-dokter-rs.html', {"idRs" : idRs, "idDoc" : idDoc, "listDoctors" : listDoctors, "listRs" : listRs})

def dokRsDelete(request):
    return redirect('rudDokterRs')

def layananJadwalPoli(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    layanan_poliklinik = 'SELECT id_poliklinik, nama, deskripsi, kode_rs_cabang FROM LAYANAN_POLIKLINIK;'
    cursor.execute(layanan_poliklinik)
    data = namedtuplefetchall(cursor)

    listOfPolikliniks = {}
    for i in data:
        listOfPolikliniks[i[0]] = [i[1], i[2], i[3]]
    #listOfPolikliniks = {
    #    "200001" : ["Orthopedic", "Menangani masalah tulang", "100004"],
    #    "200002" : ["Psychiatry", "Menangani masalah mental", "100005"],
    #    "200003" : ["Interna", "Menangani masalah penyakit dalam", "100001"],
    #    "200004" : ["Obgyn", "Menangani masalah kehamilan", "100003"],
    #    "200005" : ["Neurology", "Menangani masalah syaraf", "100002"]
    #}
    return render(request, 'layanan-jadwal-poliklinik.html', {"list" : listOfPolikliniks})

def layananJadwalPoliAdmin(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    layanan_poliklinik = 'SELECT id_poliklinik, nama, deskripsi, kode_rs_cabang FROM LAYANAN_POLIKLINIK;'
    cursor.execute(layanan_poliklinik)
    data = namedtuplefetchall(cursor)

    listOfPolikliniks = {}
    for i in data:
        listOfPolikliniks[i[0]] = [i[1], i[2], i[3]]
    #listOfPolikliniks = {
    #    "200001" : ["Orthopedic", "Menangani masalah tulang", "100004"],
    #    "200002" : ["Psychiatry", "Menangani masalah mental", "100005"],
    #    "200003" : ["Interna", "Menangani masalah penyakit dalam", "100001"],
    #    "200004" : ["Obgyn", "Menangani masalah kehamilan", "100003"],
    #    "200005" : ["Neurology", "Menangani masalah syaraf", "100002"]
    #}
    return render(request, 'layanan-jadwal-poliklinik-admin.html', {"list" : listOfPolikliniks})

def orderPoli(request):
    kodePoli = request.GET['kodePoli']
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    jadwal_poli = 'SELECT * FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_poliklinik=' + kodePoli + ';'
    cursor.execute(jadwal_poli)
    listOfSched = namedtuplefetchall(cursor)
    return render(request, "jadwal-poliklinik.html", { "listOfSched" : listOfSched })

def orderPoliAdmin(request):
    kodePoli = request.GET['kodePoli']
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO MEDIKAGO;')
    jadwal_poli = 'SELECT * FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_poliklinik=' + kodePoli + ';'
    cursor.execute(jadwal_poli)
    listOfSched = namedtuplefetchall(cursor)
    return render(request, "jadwal-poliklinik-admin.html", { "listOfSched" : listOfSched })

def updateLayananPoliklinik(request):
    #listOfPolikliniks = {
    #    "200001" : ["Orthopedic", "Menangani masalah tulang", "100004"],
    #    "200002" : ["Psychiatry", "Menangani masalah mental", "100005"],
    #    "200003" : ["Interna", "Menangani masalah penyakit dalam", "100001"],
    #    "200004" : ["Obgyn", "Menangani masalah kehamilan", "100003"],
    #    "200005" : ["Neurology", "Menangani masalah syaraf", "100002"]
    #}
    #for i in listOfPolikliniks:
    #    a = LayananPoliklinik(
    #        id_poliklinik= i,
    #        nama_layanan=listOfPolikliniks.get(i)[0],
    #        deskripsi=listOfPolikliniks.get(i)[1],
    #        rs_cabang=listOfPolikliniks.get(i)[2]
    #        )
    #    a.save()
    if request.method == 'GET':
        id_poliklinik = request.GET["kodePoli"]
        cursor1 = connection.cursor()
        cursor1.execute('SET SEARCH_PATH TO MEDIKAGO;')
        jadwal_poli = 'SELECT * FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_poliklinik=' + id_poliklinik + ';'
        cursor1.execute(jadwal_poli)
        poliklinik = namedtuplefetchall(cursor1)

        cursor2 = connection.cursor()
        cursor2.execute('SET SEARCH_PATH TO MEDIKAGO;')
        kode_rs = 'SELECT kode_rs FROM RS_CABANG;'
        cursor2.execute(kode_rs)
        listKodeRs = namedtuplefetchall(cursor2)
        
        #poliklinik = LayananPoliklinik.objects.get(id_poliklinik=id_poliklinik)
        #print(poliklinik.getId())
        form = FormUpdateLayananPoliklinik(request.GET)
        return render(request, 'update-layanan-poliklinik.html', { "listKodeRs" : listKodeRs, "poliklinik" : poliklinik, "form" : form})
    elif request.method == 'POST':
        data = request.POST.copy()
        return redirect('layananJadwalPoli')

def deleteLayananPoliklinik(request):
    # TODO update database
    return redirect('layananJadwalPoli')

def updateJadwalPoliklinik(request):
    if request.method == 'GET':
        id_jadpol = request.GET['idJadwalPoli']
        jadPol = JadwalLayananPoliklinik.objects.get(id_jadwal_poliklinik=id_jadpol)
        form = FormUpdateJadwalLayananPoliklinik(request.GET)
        return render(request, "update-jadwal-layanan-poliklinik.html", {"jadPol" : jadPol, "form" : form})
    elif request.method == 'POST':
        return redirect('layananJadwalPoli')

def deleteJadwalPoliklinik(request):
    # TODO update database
    return redirect('layananJadwalPoli')