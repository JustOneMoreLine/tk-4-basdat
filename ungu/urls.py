from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('daftar-dokter-rs-cabang', views.daftardokterrs, name='daftarDokterRs'),

    path('rud-dokter-rs', views.rudDokterRs, name='rudDokterRs'),
    path('rud-dokter-rs-admin', views.rudDokterRsAdmin, name='rudDokterRsAdmin'),
    path('dok-rs/update', views.dokRsUpdate, name='dokRsUpdate'),
    path('dok-rs/delete', views.dokRsDelete, name='dokRsDelete'),


    path('layanan-jadwal-poli', views.layananJadwalPoli, name='layananJadwalPoli'),
    path('layanan-jadwal-poli-admin', views.layananJadwalPoliAdmin, name='layananJadwalPoliAdmin'),
    path('orderPoli/pesan', views.orderPoli, name='orderPoli'),
    path('orderPoli-admin/pesan', views.orderPoliAdmin, name='orderPoliAdmin'),
    path('poli/update', views.updateLayananPoliklinik, name='updateLayananPoliklinik'),
    path('poli/delete', views.deleteLayananPoliklinik, name='deleteLayananPoliklinik'),
    path('orderPoli-admin/jadwal/update', views.updateJadwalPoliklinik, name='updateJadwalPoliklinik'),
    path('orderPoli-admin/jadwal/delete', views.deleteJadwalPoliklinik, name='deleteJadwalPoliklinik')
]